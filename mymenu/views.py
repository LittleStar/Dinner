from django.shortcuts import render
from mymenu.models import Menuname
from django.http import HttpRequest, HttpResponse
from django.template import Template


# Create your views here.

def mymenu(request):
    # print(request.POST.get('key'))
    value = Template("<a href='http://www.meishij.net' target='_blank'>美食杰</a>")
    return render(request, 'dinner.html')


def add_mymenu(request):
    if request.method == 'POST':
        name = request.POST['name']
        Menuname.objects.create(
            name=name,
        )
        return HttpResponse("添加菜名信息成功!")
    else:
        return render(request, 'add_mymenu.html', locals())


def check_mymenu(request):
    menulist = Menuname.objects.all()
    print(menulist)
    return render(request, 'check_mymenu.html', locals())
