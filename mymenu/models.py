from django.db import models


# Create your models here.

class Menuname(models.Model):
    name = models.CharField("菜品", max_length=30)

    class Meta:
        verbose_name = '菜品'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name
